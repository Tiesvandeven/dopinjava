# Managing dependencies

* OO combines state with functionality
* DOP separates them

---slide---

## Implementing save()

<pre>
<code class="hljs java" style="max-height: 100%;font-size:140%">public class Person {
		
  public void save(){
		//
  }
}
</code>
</pre>

---slide---

## Implementing save()

<pre>
<code class="hljs java" style="max-height: 100%;font-size:140%">public class Person {

  private final DataAccessHelper dataAccessHelper;

  public Person(DataAccessHelper dataAccessHelper){
  
    this.dataAccessHelper = dataAccessHelper;
	
  }

  public void save(){
  
    dataAccessHelper.save(this);
	
  }
}
</code>
</pre>

---slide---

## Implementing save()

<pre>
<code class="hljs java" style="max-height: 100%;font-size:140%">public class Person {

  public void save(){
  
    DataAccessHelper.getInstance().save(this);
	
  }
}
</code>
</pre>

---slide---

## Implementing save()

<pre>
<code class="hljs java" style="max-height: 100%;font-size:140%">public class Person {

  public void save(DataAccessHelper dataAccessHelper){
  
    dataAccessHelper.save(this);
	
  }
}
</code>
</pre>

---slide---

## Dependencies in OO

<img style="height: 700px;" src="slides/images/dependencies.svg">

Note:
@startuml
!theme hacker
class Component1 {
  usecase1()
}

class  Component2{
  usecase2()
}
class Person{
  usecase1()
  usecase2()
}


class RestHelper
class DataAccessHelper

Component1 --> Person
Component2 --> Person
Person --> RestHelper
Person --> DataAccessHelper
@enduml

---slide---

# Scaling usecases

---slide---

## Applications only grow


<img style="height: 500px;" src="slides/images/data_trap_2x.png">

---slide---

## So our OO classes grow

<img style="height: 500px;" src="slides/images/person1.svg">

Note:
@startuml
!theme hacker
class Person{
  usecase1()
  usecase2()
}

class Person^{
  usecase1()
  usecase2()
  usecase3()
  usecase4()
  usecase5()
  usecase6()
}
@enduml

---slide---

## The DOP way

* Seperate state and functionality
* Create a component for every usecase
* Pass the data as arguments

---slide---

## Putting logic in components

<pre>
<code class="hljs java" style="max-height: 100%;font-size:140%">public class PersonPersisterDatabase {

  public void persist(Person person){
  
    dataAccessHelper.save(person);
	
  }
}

public class PersonPersisterRest {

  public void persist(Person person){
  
    restHelper.save(person);
	
  }
}
</code>
</pre

---slide---

## But this looks familiar...

---slide---

## Dependencies in DOP

<img style="height: 700px;" src="slides/images/components.svg">


Note:
@startuml
!theme hacker
class Component1 {
  usecase1()
}

class  Component2{
  usecase2()
}
class Person
class RestHelper
class DataAccessHelper

Component1 --> Person
Component2 --> Person
Component2 -up-> RestHelper
Component1 -up-> DataAccessHelper
@enduml

---slide---

## Should you combine state and functionality?

* Be careful with dependencies
* Be careful with businesslogic
* Be pragmatic not dogmatic

---slide---

# DOP and OO patterns

---slide---

## Polymorphism

* Sharing type
* Strategy pattern

---slide---

## Strategy in OO

<pre>
<code class="hljs java" style="max-height: 100%;font-size:140%">public interface CreditCard {
  void pay();
}

public class MasterCard implements CreditCard{
  @Override
  public void pay() {}
}

public class VisaCard implements CreditCard{
  @Override
  public void pay() {}
}

public void doLogic(CreditCard creditCard){
  creditCard.pay();
}
</code>
</pre>

---slide---

## Strategy in DOP

<pre>
<code class="hljs java" style="max-height: 100%;font-size:140%">public interface CreditCard { }

public class MasterCard implements CreditCard { }

public class VisaCard implements CreditCard { }

public void doLogic(CreditCard creditCard){

  switch (creditCard){
  
    case MasterCard mc -> MasterCardPayment.pay(mc);
	
    case VisaCard vc -> VisaCardPayment.pay(vc);
	
    default -> throw new IllegalStateException();
  }
}
</code>
</pre>

---slide---

## Introducing sealed classes

<pre>
<code class="hljs java" style="max-height: 100%;font-size:140%">public sealed interface CreditCard permits VisaCard, MasterCard { }

public final class MasterCard implements CreditCard { }

public final class VisaCard implements CreditCard { }

public void doLogic(CreditCard creditCard){

  switch (creditCard){
  
    case MasterCard mc -> MasterCardPayment.pay(mc);
	
    case VisaCard vc -> VisaCardPayment.pay(vc);
  }
}
</code>
</pre>
