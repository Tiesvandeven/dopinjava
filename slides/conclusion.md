# Conclusion

* Applications do data transformation
* Treating data as data encourages this
* Dependencies matter, so be careful with mixing state and functionality
* We can still do OO patterns

---slide---

>> "Don't be a Functional programmer, don't be an Object Oriented programmer, Don't be a Data Oriented Programmer, be a _better_ programmer"

- Brian Goetz + Ties van de Ven

---slide---

## Keep in touch

<img style="float: right;" src="slides/images/ties.jpg">

* ties_ven @Twitter
* www.tiesvandeven.nl
* Or talk to me in real life
* Sheets: https://tiesvandeven.gitlab.io/dopinjava/