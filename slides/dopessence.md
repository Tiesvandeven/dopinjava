## Its all about the data

* Most applications are data streams
* Most software just does data transformation

---slide---

<img style="height: 300px;" src="slides/images/source_sink.svg">

Note:
@startuml
!theme hacker
object Source
object Sink

Source -> Sink
@enduml

---slide---

<img style="height: 500px;" src="slides/images/source_sink_operations.svg">

Note:
@startuml
!theme hacker
left to right direction

object Source
object Sink1
object Sink2
object Map
object Filter
object Branch

Source --> Map
Map --> Filter
Filter --> Branch
Branch --> Sink1
Branch --> Sink2
@enduml

---slide---

<img style="height: 400px;" src="slides/images/source_sink_application.svg">

Note:
@startuml
!theme hacker
left to right direction
skinparam nodesep 25
skinparam ranksep 30

object SourceUser
object FilterSecurity
object FilterInput
object MapToQuery
object MapToEntity
object MapToResponse
object Repository
object SinkUser

SourceUser--> FilterSecurity
FilterSecurity--> FilterInput
FilterInput --> MapToQuery
MapToQuery--> MapToEntity
MapToEntity -> Repository
MapToEntity--> MapToResponse
MapToResponse --> SinkUser
@enduml

---slide---

### Data as Data

* Use structures that are made for data transformation
	* Maps
	* Lists
* Types can get in the way

<img style="height: 500px;float: right;" src="slides/images/data.jpg">

---slide---

### JSON to Type

<img style="height: 400px;" src="slides/images/jsontoclass.svg">

Note:
@startuml
!theme hacker

<style>
json {
  BackGroundColor #d3f198
}
map {
  BackGroundColor #d3f198
}
</style>

json JSON {
   "name":"Bob",
   "age":20
}

map HashMap {
 name => Bob
 age=> 20

}

class Person {
  String name
  int age
}

JSON -> HashMap
HashMap -> Person
@enduml

---slide---

### Should you still use types?

* Data transformations are harder
* Compile time safety
* Less testing
* Use types in Java

---slide---

### Types

* It's about the primitives
* Class is a specification

<pre>
<code class="hljs java" style="max-height: 100%;font-size:140%">public record Person(
  int age, 
  String name, 
  String username, 
  String password
) { }

public record NameAge(String name, int age){}

public record UserNamePassword(String username, String password){}
</code>
</pre>

---slide---

### Prerequisites of DOP

* Validate data at edges
* Immutability

---slide---

## Benefits of data as data

* Performance
* Security
* Managing dependencies

---slide---

## Performance

<img style="height: 400px;" src="slides/images/optimise.svg">

<br>

* Transform data to optimize for usecases

Note:
@startuml
!theme hacker

object "List<Person>" as P
object "PersonData" as E

E : String[] names
E : int[] ages

P -> E

@enduml
</code>
</pre>

---slide---

## Security

* Least amount of information
* Logger friendly
* Serialize friendly