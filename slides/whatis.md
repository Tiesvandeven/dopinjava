# Object Oriented 
vs
# Functional Programming
vs
# Data Oriented Programming

---slide---

## What is OO

* Everything is an object
* Encapsulation
* Boundaries
* Inheritance

---slide---

## What is OO

<pre>
<code class="hljs java" style="max-height: 100%;font-size:140%" data-line-numbers="1-10|1,5,7,9">public class Person {
  private int age;
  private String name;

  public int getAge() { return age; }

  public String getName() { return name; }
		
  public void doBusinessLogic() {}
}
</code>
</pre>

---slide---

## What is FP

* Everything is a function
* Higher order functions
* Building blocks
* Composition

---slide---

## What is DOP

* Similar to FP
* Everything is data

---slide---

## What is DOP

<pre>
<code class="hljs java" style="max-height: 100%;font-size:140%" data-line-numbers="1-6|2,3">public record Person(
  int age,
  String name
){}
</code>
</pre>
